import * as cdk from 'aws-cdk-lib';
import { getConfig } from '../config/build-config';
import { VpcStack } from './stack/vpc-stack';
import { ApiGatewayStack } from './stack/apigw-stack';

export class ServerlessCdkStack extends cdk.Stack {
    private readonly config;

    constructor(scope: cdk.App, id: string) {
        super(scope, id, {
            stackName: ['serverless', 'master', 'stack'].join('-'),
        });

        this.config = getConfig(scope);

        // VPC Stack
        const vpcStack = new VpcStack(
            scope,
            'VpcStack',
            this.config.projectName,
            this.config.environment,
        );
        // APIgw
        new ApiGatewayStack(
            scope,
            'APIGWStack',
            this.config.projectName,
            this.config.environment,
        );
    }
}
