import * as ec2 from 'aws-cdk-lib/aws-ec2';
import { Construct } from 'constructs';
import { CommonTags } from './abstract/naming-conventions';
export class Vpc extends CommonTags {
    public vpc: ec2.CfnVPC;

    constructor(scope: Construct, projectName: string, stageName: string) {
        super();
        this.vpc = new ec2.CfnVPC(scope, `VPC`, {
            cidrBlock: '10.90.16.0/22',
            enableDnsSupport: true,
            enableDnsHostnames: true,
            instanceTenancy: `default`,
            tags: [
                {
                    key: `Name`,
                    value: this.createTagName(scope, projectName, stageName, 'vpc'),
                },
            ],
        });
    }
}
