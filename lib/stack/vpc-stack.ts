import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { Vpc } from '../resource';

export class VpcStack extends cdk.Stack {
    public readonly vpc: Vpc;

    constructor(scope: Construct, id: string, projectName: string, stageName: string) {
        super(scope, id, {
            stackName: [projectName, stageName, 'net'].join('-'),
        });

        this.vpc = new Vpc(this, projectName, stageName);
    }
}
