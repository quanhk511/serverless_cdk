import * as apigateway from 'aws-cdk-lib/aws-apigateway';
import { Construct } from 'constructs';
import { CommonTags } from './abstract/naming-conventions';
import { aws_sam as sam } from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda';
const fs = require('fs');

export interface ApiGatewayResource {
    readonly ParentId: string;
    readonly PathPart: string;
    readonly RestApiId: string;
    readonly assign: (apiResource: apigateway.CfnResource) => void;
}

export class ApiGateway extends CommonTags {
    public api: sam.CfnApi;

    constructor(scope: Construct, projectName: string, stageName: string) {
        super();
        this.api = new sam.CfnApi(scope, 'api', {
            stageName: stageName,
            auth: {
                addDefaultAuthorizerToCorsPreflight: false,
            },
            definitionBody: JSON.parse(fs.readFileSync('lib/resource/script/oapi.json')),
            openApiVersion: '3.0',
            endpointConfiguration: {
                type: 'EDGE'
            },
            tags: {
                key: `Name`,
                value: this.createTagName(scope, projectName, stageName, 'apigw'),
            },
        });
    }
    private lambdaAuthen(scope: Construct) {
        new lambda.Function(scope, 'MyFunction', {
            runtime: lambda.Runtime.PYTHON_3_7,
            handler: 'app.lambda_handler',
            code: lambda.Code.fromAsset('./my_function'),
          });
    }
}
