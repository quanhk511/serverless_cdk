import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { ApiGateway } from '../resource';

export class ApiGatewayStack extends cdk.Stack {
    public readonly apigw: ApiGateway;

    constructor(scope: Construct, id: string, projectName: string, stageName: string) {
        super(scope, id, {
            stackName: [projectName, stageName, 'apigw'].join('-'),
        });

        this.apigw = new ApiGateway(this, projectName, stageName);
    }
}
