#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { ServerlessCdkStack } from '../lib/serverless_cdk-stack';
import { getConfig } from '../config/build-config';

const app = new cdk.App();
const config = getConfig(app);

new ServerlessCdkStack(app, 'ServerlessCdkStack', {
  stackName: 'cdk-stack',
  // env account.
  env: {
    region: process.env.CDK_DEFAULT_REGION,
    account: process.env.CDK_DEFAULT_ACCOUNT,
  },
  // common tags for project
  tags: {
    CostCenter: config.costOwner,
    Project: config.projectName,
    ProjectOwner: config.projectOwner,
  },
});