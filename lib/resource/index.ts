import { Vpc } from './vpc';
import { ApiGateway } from './api-gateway';
export { Vpc, ApiGateway };
